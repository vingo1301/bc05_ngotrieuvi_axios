const BASE_URL = "https://635f4c4b3e8f65f283b02e50.mockapi.io/";
var idEdited = null;
// render all todos services
function fetchAllTodo(){
    axios({
        url: `${BASE_URL}/project`,
        method: "GET"
    })
    .then(function(res){
        turnOffLoading();
        renderToDoList(res.data);
    }).catch(function(err){
        turnOffLoading();
        console.log("err: ", err);
        }
    );
}
fetchAllTodo();
// remove todo service
function removeTodo(idTodo){
    turnOnLoading();
    axios({
        url: `${BASE_URL}/project/${idTodo}`,
        method: "DELETE",
    })
    .then(function(res){
        fetchAllTodo();
        console.log("res: ",res);
    })
    .catch(function(err){
        console.log("err: "),err;
    })
}
function addTodo(){
    var data = layThongTinTuForm();    

    var newTodo = {
        name: data.name,
        desc: data.desc,
        isComplete: true,
    };
    turnOnLoading();
    axios({
        url: `${BASE_URL}/project`,
        method: "POST",
        data:newTodo
    })
    .then(function(res){
        fetchAllTodo();
        console.log("res: ",res);
    })
    .catch(function(err){
        console.log("err: "),err;
    })
}
function editTodo(idTodo){
    turnOnLoading();
    axios({
        url: `${BASE_URL}/project/${idTodo}`,
        method: "GET",
    })
    .then(function(res){
        // fetchAllTodo();
        // show thông tin lên form
        turnOffLoading();
        document.getElementById("name").value =res.data.name; 
        document.getElementById("desc").value =res.data.desc; 
        idEdited = res.data.id;
    })
    .catch(function(err){
        turnOffLoading();
        console.log("err: "),err;
    })
}
function updateTodo(){
    var data = layThongTinTuForm();
    axios({
        url: `${BASE_URL}/project/${idEdited}`,
        method: "PUT",
        data: data,
    })
    .then(function(res){
        console.log("res: ",res);
        fetchAllTodo();
        
    })
    .catch(function(err){
        console.log("err: "),err;
    })
}